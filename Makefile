export APP_MODE=docker

start:
	docker-compose -f app-compose.yaml up -d

db:
	docker-compose -f db-compose.yaml up -d

stop:
	docker-compose -f app-compose.yaml down