# DATATRONiQ test task

![datatroniq main logo image](https://berlinstartupjobs.com/wp-content/uploads/Logo_Datatroniq_Ansicht_Suchergebnis-e1692970466707.png "Main logo")

## Prepare

> install Docker desktop for
>
> [mac](https://docs.docker.com/desktop/install/mac-install/)
>
> [windows](https://docs.docker.com/desktop/install/windows-install/)
>
> [linux](https://docs.docker.com/desktop/install/linux-install/)

> install "make" sys lib for your system (just google it)

### Running Mode

open terminal and run it with follow command:
> make start

or if you don't have `make` just run it with docker-compose:
> docker-compose -f app-compose.yaml up -d

**Stop**
> make stop

or if you don't have `make` just run it with docker-compose:
> docker-compose -f app-compose.yaml down

### Development Mode

Open 2 terminals and run back and front offices:

**Backend**
> npm run start:dev

**Frontend**
> npm run start

## ! Using file !
> Please use csv or xlsx file from ./EmployeeSampleData directory

# Implemented features:
 - ### Mandatory part
   - [x] File upload (to server)
   - [x] Display data
   - [x] Sorting/Filtering (server side)
   - [x] Webpack
   - [x] ESLint (Server/Front)
   - [ ] Unit test
   - [x] Instruction in README.md
   - [x] Libraries/Frameworks explanations
   - [ ] TypeScript gotchas
   - [x] Styling to the application
   - [x] Responsive design
   - [x] Animations or transitions
 - ### Optional part
   - [x] Pagination (server side)
   - [x] Persistent (Postgres)
   - [x] Changing data
   - [x] Auth
   - [ ] e2e
   - [ ] new extra features
 - ### Bonus part
   - [x] Dockerfile
   - [x] docker-compose
   - [x] Readiness/Liveness health checks
   - [x] Dockerfile best practice
   - [ ] K8s
## Stack and libs

## Explanations Libs/Frameworks
### Backend
- Nestjs - Modern web framework with modularity base, CLI, TS support, Dependencies Injection, REST/GraphQL/Websocket/Microservices supports. 
- Postgres - relations database, base on my prev experience
- xlsx - universal popular library for working with xlsx documents (csv includes)
### Frontend
- React - Most popular library (big community, esy to use)
- Webpack - Most popular web builder, base on prev experience
- Redux Toolkit - Common state persister for the web
- Clean Architecture pattern (flexible to use and update business logic)
- MUI + mui-x-data-grid - made by Google popular design system (prev. MaterialUI)
### Infrastructure
- Docker (Dockerfile/docker compose)

> ## Possible Future features
> #### Backend
> - Pub/Sub (RabbitMQ); Websocket
> - Elastic search
> - Advanced user model (creds/roles)
> - Advanced auth flow like (OAuth 2.0, 2FA)
> - SSL
> - Add/Remove employee
> - Lock for DB changing by ID
> - Add file validation
> - not uniq ids problem
> - get total count from DB
> #### Frontend
> - Websocket; Pub/Sub
> - SSL
> - Advanced API doc (swagger)
> - Use predictable (structured) API like GraphQL, JSON API Schema, gRPC...
> - Export to file feature
> - Hide/Give upload file if data exists 
> - file size validation ?
> - split file from employee slice ?
> - errors cleaning flow
> - Create employee