import { useDispatch, useSelector } from 'react-redux'
import type { TypedUseSelectorHook } from 'react-redux'
import type { RootState, AppDispatch } from './redux'

// Use throughout your app instead of plain `useDispatch` and `useSelector`
type DispatchFunc = () => AppDispatch
export const useAppDispatch: DispatchFunc = useDispatch
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector

export enum FetchingStatus {
    Idle = "idle",
    Loading = "loading",
    Succeeded = "succeeded",
    Failed = "failed"
}

export interface IFetchingStatus {
    status: FetchingStatus
    error: string | null
}
