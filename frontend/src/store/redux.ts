import { configureStore } from '@reduxjs/toolkit'
import logger from 'redux-logger'

import authReducer from '@root/features/Auth/Store/AuthSlice'
import employeeReducer from '@root/features/Employees/Store/EmployeeSlice'

const isSnowpackDevMode = process.env.NODE_ENV === 'development';

export const store = configureStore({
    middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware({serializableCheck: false})
            // prepend and concat calls can be chained
            .concat(logger),
    reducer: {
        auth: authReducer,
        employees: employeeReducer,
    },
    devTools: isSnowpackDevMode,
})

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch
