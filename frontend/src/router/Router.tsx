import React from "react";
import {createBrowserRouter} from "react-router-dom";

import { App } from '@root/App'
import {SignupView} from "@root/features/Auth/View";
import {EmployeesView} from "@root/features/Employees/View/EmployeesView";
import {LoginView} from "@root/features/Auth/View/LoginView";

export const mainRouter = createBrowserRouter([
    {
        index: true,
        element: <h1>Main</h1>,
        errorElement: <div>Page not found</div>,
    },
    {
        path: "dashboard",
        element: <App />,
        errorElement: <div>Page not found</div>,
        children: [
            {
                index: true,
                element: <EmployeesView />
            }
        ]
    },
    {
        path: "signup",
        element: <SignupView />
    },
    {
        path: "login",
        element: <LoginView />
    }
]);
