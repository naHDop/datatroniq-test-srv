import {useCallback} from "react";

import {IAuthStore} from "@root/features/Auth/Domain/AuthStore";
import {
    resetUseCase,
    setEmailUseCase,
    setPasswordUseCase,
    setAuthStatusUseCase,
    signupUseCase,
    loginUseCase,
    clearErrorUseCase,
} from "@root/features/Auth/UseCase/AuthUseCase";
import {AuthStatus, AuthStatusComplete} from "@root/features/Auth/Domain/AuthEntity";
import {FetchingStatus} from "@root/store/contract";

export function useAuthViewModel(store: IAuthStore) {
    const setEmail = useCallback((email: string) => {
        return setEmailUseCase(store, email)
    }, [store])

    const setPassword = useCallback((password: string) => {
        return setPasswordUseCase(store, password)
    }, [store])

    const setAuthStatus = useCallback((status: AuthStatus) => {
        return setAuthStatusUseCase(store, status)
    }, [store])

    const resetStore = useCallback(() => {
        return resetUseCase(store)
    }, [store])

    const clearError = useCallback(() => {
        return clearErrorUseCase(store)
    }, [store])

    const signup = useCallback(() => {
        return signupUseCase(store)
    }, [store])

    const login = useCallback(() => {
        return loginUseCase(store)
    }, [store])

    return {
        showPreloader: store.status === FetchingStatus.Loading,
        isAuthComplete: store.authStatus === AuthStatusComplete,
        email: store.entity.email,
        password: store.entity.password,
        isError: store.error != null,
        error: store.error,
        clearError,
        setEmail,
        setPassword,
        setAuthStatus,
        resetStore,
        signup,
        login,
    }
}