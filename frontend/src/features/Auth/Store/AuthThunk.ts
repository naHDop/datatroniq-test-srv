import {createAsyncThunk} from "@reduxjs/toolkit";
import {AppDispatch} from "@root/store/redux";

import {InitialStateType, setAuthStatus} from "@root/features/Auth/Store/AuthSlice";
import {LOG_IN_THUNK, SIGN_UP_THUNK} from "@root/features/Auth/Store/AuthContract";
import {IAuthEntity, AuthStatus, AuthStatusPending} from "@root/features/Auth/Domain/AuthEntity";
import {signupApi, loginApi} from "@root/features/Auth/Driver/AuthApi";

export const signupThunk = createAsyncThunk<
    // Return type of the payload creator
    AuthStatus,
    // First argument to the payload creator
    IAuthEntity,
    {
        // Optional fields for defining thunkApi field types
        dispatch: AppDispatch
        state: InitialStateType
    }
>(SIGN_UP_THUNK, async (entity: IAuthEntity, thunkApi) => {
    thunkApi.dispatch(setAuthStatus(AuthStatusPending))
    const res = await signupApi(entity)

    return res.status
})

export const loginThunk = createAsyncThunk<
    // Return type of the payload creator
    { token: string },
    // First argument to the payload creator
    IAuthEntity,
    {
        // Optional fields for defining thunkApi field types
        dispatch: AppDispatch
        state: InitialStateType
    }
>(LOG_IN_THUNK, async (entity: IAuthEntity, thunkApi) => {
    thunkApi.dispatch(setAuthStatus(AuthStatusPending))
    return await loginApi(entity)
})
