import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {RootState} from "@root/store/redux";

import {IAuthStore} from "@root/features/Auth/Domain/AuthStore";
import {FetchingStatus} from "@root/store/contract";
import {AuthStatus, AuthStatusComplete, AuthStatusIdle} from "@root/features/Auth/Domain/AuthEntity";
import {SIGN_UP_SLICE} from "@root/features/Auth/Store/AuthContract";
import {loginThunk, signupThunk} from "@root/features/Auth/Store/AuthThunk";

export type InitialStateType = Omit<IAuthStore, 'cleanError' | 'login' | 'setEmail' | 'setPassword' | 'setAuthStatus' | 'resetStore' | 'signup'>

const initialState: InitialStateType = {
    entity: {
        email: null,
        password: null,
    },
    error: null,
    authStatus: AuthStatusIdle,
    status: FetchingStatus.Idle
}

export const authSlice = createSlice({
    name: SIGN_UP_SLICE,
    initialState,
    reducers: {
        resetStore: (state: InitialStateType) => {
            state.entity.email = null
            state.entity.password = null
            state.error = null
        },
        setAuthStatus: (state: InitialStateType, action: PayloadAction<AuthStatus>) => {
            state.authStatus = action.payload
        },
        setEmail: (state: InitialStateType, action: PayloadAction<string>) => {
            state.entity.email = action.payload
        },
        setPassword: (state: InitialStateType, action: PayloadAction<string>) => {
            state.entity.password = action.payload
        },
        cleanError(state: InitialStateType) {
            state.error = null
        },
    },
    extraReducers: (build) => {
        build
            .addCase(loginThunk.pending, (state: InitialStateType) => {
                state.status = FetchingStatus.Loading
            })
            .addCase(loginThunk.fulfilled, (state: InitialStateType, action: PayloadAction<{token: string}>) => {
                state.status = FetchingStatus.Succeeded
                state.authStatus = AuthStatusComplete
                localStorage.setItem('token', action.payload.token)
            })
            .addCase(loginThunk.rejected, (state: InitialStateType, action) => {
                state.status = FetchingStatus.Failed
                if (action.error?.message) {
                    state.error = action.error.message
                } else {
                    state.error = "Unknown error"
                }
            })

            .addCase(signupThunk.pending, (state: InitialStateType) => {
                state.status = FetchingStatus.Loading
            })
            .addCase(signupThunk.fulfilled, (state: InitialStateType, action: PayloadAction<AuthStatus>) => {
                state.status = FetchingStatus.Succeeded
                state.authStatus = action.payload
            })
            .addCase(signupThunk.rejected, (state: InitialStateType, action) => {
                state.status = FetchingStatus.Failed
                if (action.error?.message) {
                    state.error = action.error.message
                } else {
                    state.error = "Unknown error"
                }
            })
    }
})

export const { setEmail, setPassword, setAuthStatus, resetStore, cleanError } = authSlice.actions
export const authSelector = (state: RootState) => state.auth as InitialStateType;
export default authSlice.reducer