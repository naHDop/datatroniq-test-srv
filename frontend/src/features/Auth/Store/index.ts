import {useCallback} from "react";

import {IAuthStore} from "@root/features/Auth/Domain/AuthStore";
import {authSelector} from "@root/features/Auth/Store/AuthSlice";
import {useAppDispatch, useAppSelector} from "@root/store/contract";
import { 
    setEmail as setEmailSlice,
    setPassword as setPasswordSlice,
    setAuthStatus as setAuthStatusSlice,
    resetStore as resetStoreSlice,
    cleanError as cleanErrorSlice,
} from '@root/features/Auth/Store/AuthSlice'
import {AuthStatus} from "@root/features/Auth/Domain/AuthEntity";
import {signupThunk, loginThunk} from "@root/features/Auth/Store/AuthThunk";

export function useAuthStore(): IAuthStore {
    const store = useAppSelector(authSelector)
    const { entity, authStatus, status, error } = store
    const dispatch = useAppDispatch()

    const signup = useCallback(() => {
        return dispatch(signupThunk(entity))
    }, [dispatch, entity])

    const login = useCallback(() => {
        return dispatch(loginThunk(entity))
    }, [dispatch, entity])

    const setEmail = useCallback((email: string) => {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        return dispatch(setEmailSlice(email))
    }, [dispatch])

    const setPassword = useCallback((password: string) => {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        return dispatch(setPasswordSlice(password))
    }, [dispatch])

    const cleanError = useCallback(() => {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        return dispatch(cleanErrorSlice())
    }, [dispatch])

    const setAuthStatus = useCallback((status: AuthStatus) => {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        return dispatch(setAuthStatusSlice(status))
    }, [dispatch])

    const resetStore = useCallback(() => {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        return dispatch(resetStoreSlice())
    }, [dispatch])
    
    return {
        entity,
        authStatus,
        status,
        cleanError,
        error,
        setEmail,
        setPassword,
        setAuthStatus,
        resetStore,
        signup,
        login,
    }
}