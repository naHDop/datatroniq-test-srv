import {AxiosResponse} from "axios";

import {ApiClient} from "@root/clients/api";
import {IAuthEntity, AuthStatus, AuthStatusComplete} from "@root/features/Auth/Domain/AuthEntity";
import {GenericResponse} from "@root/contracts/clients/api/http";

export function signupApi(dto: IAuthEntity): Promise<{status: AuthStatus}> {
    return new Promise((resolve, reject) => {
        ApiClient.post<
            IAuthEntity,
            AxiosResponse<GenericResponse<null>>
        >("/v1/signup", dto)
            .then(() => {
                resolve({ status: AuthStatusComplete })
            })
            .catch(err => {
                reject(err)
            })
    })
}

export function loginApi(dto: IAuthEntity): Promise<{token: string}> {
    return new Promise((resolve, reject) => {
        ApiClient.post<
            IAuthEntity,
            AxiosResponse<GenericResponse<{token: string}>>
        >("/v1/login", dto)
            .then(({ data }) => {
                resolve({ token: data.data?.token })
            })
            .catch(err => {
                reject(err)
            })
    })
}
