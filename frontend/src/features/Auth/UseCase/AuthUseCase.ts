import {IAuthStore} from "@root/features/Auth/Domain/AuthStore";
import {AuthStatus} from "@root/features/Auth/Domain/AuthEntity";

export const setEmailUseCase = (store: IAuthStore, email: string) => {
    // TODO: add validate email
    return store.setEmail(email)
}

export const setPasswordUseCase = (store: IAuthStore, password: string) => {
    // TODO: add validate password 
    return store.setPassword(password)
}

export const setAuthStatusUseCase = (store: IAuthStore, status: AuthStatus) => {
    return store.setAuthStatus(status)
}

export const clearErrorUseCase = (store: IAuthStore) => {
    return store.cleanError()
}

export const resetUseCase = (store: IAuthStore) => {
    return store.resetStore()
}

export const signupUseCase = (store: IAuthStore) => {
    return store.signup()
}

export const loginUseCase = (store: IAuthStore) => {
    return store.login()
}