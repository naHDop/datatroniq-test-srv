import React, {JSX, useCallback, useEffect} from "react";
import {useNavigate} from "react-router-dom";
import {toast} from "react-toastify";

import {useAuthStore} from "@root/features/Auth/Store";
import {useAuthViewModel} from "@root/features/Auth/Controller";
import {AuthForm} from "@ui/AuthForm/AuthForm";

export function SignupView(): JSX {
    const store = useAuthStore()
    const {
        email,
        password,
        setEmail,
        error,
        clearError,
        isError,
        showPreloader,
        setPassword,
        signup,
        resetStore,
    } = useAuthViewModel(store)
    const navigateTo = useNavigate()

    const confirmHandler = useCallback(async () => {
        if (email && password) {
            const res = await signup()
            if (!res.hasOwnProperty('error')) {
                resetStore()
                navigateTo('/login')
            }
        }
    }, [email, password])

    useEffect(() => {
        if (error) {
            toast(error, { type: 'error' })
            clearError()
        }
    }, [error])

    return (
        <AuthForm
            emailValue={email || ""}
            header="Sign Up"
            buttonLabel="Sign Up"
            onEmailChange={setEmail}
            onPasswordChange={setPassword}
            onConfirm={confirmHandler}
            isError={isError}
            {...(isError ? {error: "Please use different email"} : {})}
            isLoading={showPreloader}
            onNavigate={() => navigateTo('/login')}
            authAccountExistsText="Already have an account?"
        />
    )
}