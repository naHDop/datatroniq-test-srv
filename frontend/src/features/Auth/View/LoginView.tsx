import React, {JSX, useCallback, useEffect} from "react";
import {useAuthStore} from "@root/features/Auth/Store";
import {useAuthViewModel} from "@root/features/Auth/Controller";
import {AuthForm} from "@ui/AuthForm/AuthForm";
import {useNavigate} from "react-router-dom";
import {toast} from "react-toastify";

export function LoginView(): JSX {
    const store = useAuthStore()
    const { 
        email,
        password,
        setEmail,
        isError,
        clearError,
        error,
        showPreloader,
        setPassword,
        login,
        resetStore
    } = useAuthViewModel(store)
    const navigateTo = useNavigate()

    const confirmHandler = useCallback(async () => {
        if (email && password) {
            const res = await login()
            if (!res.hasOwnProperty('error')) {
                resetStore()
                navigateTo('/dashboard')
            }
        }
    }, [email, password])
    
    useEffect(() => {
        if (error) {
            toast(error, { type: 'error' })
            clearError()
        }
    }, [error])

    return (
        <AuthForm
            emailValue={email || ""}
            header="Log In"
            buttonLabel="Log In"
            onEmailChange={setEmail}
            onPasswordChange={setPassword}
            onConfirm={confirmHandler}
            isError={isError}
            {...(isError ? {error: "Please use different email"} : {})}
            isLoading={showPreloader}
            onNavigate={() => navigateTo('/signup')}
            authAccountExistsText="Don't have an account?"
        />
    )
}