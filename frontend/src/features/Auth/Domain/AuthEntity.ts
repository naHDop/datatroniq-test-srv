import { ProcessStatus } from '@root/contracts/enums'

export type AuthStatus = ProcessStatus
export const AuthStatusIdle: AuthStatus = ProcessStatus.RegistrationIdle
export const AuthStatusPending: AuthStatus = ProcessStatus.RegistrationPending
export const AuthStatusFailed: AuthStatus = ProcessStatus.RegistrationFailed
export const AuthStatusComplete: AuthStatus = ProcessStatus.RegistrationComplete


export interface IAuthEntity {
    email: string | null
    password: string | null
}