import {IFetchingStatus} from "@root/store/contract";
import {IAuthEntity, AuthStatus} from "@root/features/Auth/Domain/AuthEntity";

export interface IAuthStore extends IFetchingStatus {
    entity: IAuthEntity
    authStatus: AuthStatus

    setEmail(email: string)
    cleanError()
    setPassword(password: string)
    setAuthStatus(s: AuthStatus)
    signup()
    login()
    resetStore()
}