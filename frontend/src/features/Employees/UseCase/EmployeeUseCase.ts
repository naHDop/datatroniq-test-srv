import {toast} from "react-toastify";

import {IEmployeeStore} from "@root/features/Employees/Domain/EmployeeStore";
import {
    IEmployeeEntity,
    ILoadingParams,
    SearchOperator,
    SortOrder
} from "@root/features/Employees/Domain/EmployeeEntity";
import {GridFilterModel, GridSortModel} from "@mui/x-data-grid";

export const updateEmployeeUseCase = (store: IEmployeeStore, dto: IEmployeeEntity) => {
    return store.updateEmployee(dto)
}

export const deleteEmployeeUseCase = async (store: IEmployeeStore, id: string) => {
    if (!store.entity) {
        toast("no entities", { type: 'error' })
        return
    }
    const employee = store.entity.find(e => e.id === id)
    if (!employee) {
        toast("employee not exists", { type: 'error' })
        return
    }
    const res = await store.deleteEmployee(employee)
    if (!res.hasOwnProperty('error')) {
        toast(`entity ${id} deleted`, { type: 'success' })
        return loadEmployeesUseCase(store)
    }
}

export const createEmployeeUseCase = (store: IEmployeeStore, dto: IEmployeeEntity) => {
    return store.createEmployee(dto)
}

export const sortModelChangeUseCase = (store: IEmployeeStore, sortModel: GridSortModel) => {
    const item = sortModel[0]
    if (item?.field && item?.sort) {
        store.setSort({
            sortBy: item.field,
            sortOrder: item.sort.toUpperCase() as SortOrder,
        })
        return loadEmployeesUseCase(store, { sortOrder: item.sort.toUpperCase() as SortOrder, sortBy: item.field })
    } else {
        return loadEmployeesUseCase(store)
    }
}

export const loadFilteredEmployeesUseCase = async (store: IEmployeeStore, model: GridFilterModel) => {
    const item = model.items[0]
    if (!item) {
        toast("item not found", { type: 'warning' })
    }
    if (item.value && item.value?.length > 1) {
        const filter = { operator: item.operator as SearchOperator, searchString: item.value, field: item.field}
        store.setFilter(filter)
        const res = await loadEmployeesUseCase(store, filter)
        if (res.error) {
            toast(res.error.message, { type: 'error' })
            store.cleanError()
        }
        return res
    } else {
        const filter = { operator: '' as SearchOperator, searchString: '', field: ''}
        store.setFilter(filter)
        const res = await loadEmployeesUseCase(store, filter)
        if (res.error) {
            toast(res.error.message, { type: 'error' })
            store.cleanError()
        }
        return res
    }
}

export const loadNextEmployeesUseCase = (store: IEmployeeStore, pagination: {page: number, pageSize: number}) => {
    store.setOffset(pagination.page)
    return loadEmployeesUseCase(store, {
        offset: pagination.page
    })
}


export const loadEmployeesUseCase = (store: IEmployeeStore, pagination?: ILoadingParams) => {
    return store.loadEmployees({ 
        offset: !!pagination?.offset ? pagination.offset : store.offset, 
        limit: !!pagination?.limit ? pagination.limit : store.limit,
        operator: !!pagination?.operator ? pagination.operator : store.operator,
        field: !!pagination?.field ? pagination.field : store.field,
        searchString: !!pagination?.searchString ? pagination.searchString : store.searchString,
        sortBy: !!pagination?.sortBy ? pagination.sortBy : undefined,
        sortOrder: !!pagination?.sortOrder ? pagination.sortOrder : undefined,
    })
}

export const cleanErrorUseCase = (store: IEmployeeStore) => {
    return store.cleanError()
}

export const uploadFileUseCase = async (store: IEmployeeStore, file: File) => {
    const allowedExtensions = ['.csv', '.xlsx'];
    const extension = file.name.slice(((file.name.lastIndexOf(".") - 1) >>> 0) + 2);
    if (allowedExtensions.includes(`.${extension}`)) {
        const res = await store.uploadFile(file)
        if (res.error) {
            toast(res.error.message, { type: 'error' })
            store.cleanError()
        } else {
            loadEmployeesUseCase(store)
        }
        return res
    } else {
        toast('Please select a valid CSV or XLSX file.', {type: "error"});
    }
}

export const serverUpdateEmployeeUseCase = async (store: IEmployeeStore, newEntity: IEmployeeEntity, originEntity: IEmployeeEntity) => {
    const res = await store.serverUpdateEmployee(newEntity)
    if (res.error) {
        toast(res.error.message, { type: 'error' })
        store.cleanError()
        return Promise.resolve(originEntity)
    }
    toast(`entity ${newEntity.id} has changed`, { type: 'success' })
    return Promise.resolve(newEntity)
}

export const setLimitUseCase = (store: IEmployeeStore, limit: number) => {
    return store.setLimit(limit)
}

export const setFilterUseCase = (store: IEmployeeStore, filter: Omit<ILoadingParams, 'offset' | 'limit' | 'sortBy' | 'sortOrder'>) => {
    return store.setFilter(filter)
}

export const setSortUseCase = (store: IEmployeeStore, sort: Omit<ILoadingParams, 'offset' | 'limit' | 'searchString' | 'operator' | 'field'>) => {
    return store.setSort(sort)
}

export const setOffsetUseCase = (store: IEmployeeStore, offset: number) => {
    return store.setOffset(offset)
}