import {useCallback} from "react";

import {IEmployeeStore} from "@root/features/Employees/Domain/EmployeeStore";
import {FetchingStatus} from "@root/store/contract";
import {IEmployeeEntity, ILoadingParams} from "@root/features/Employees/Domain/EmployeeEntity";
import {
    cleanErrorUseCase,
    loadEmployeesUseCase,
    serverUpdateEmployeeUseCase,
    setLimitUseCase,
    setOffsetUseCase,
    updateEmployeeUseCase,
    uploadFileUseCase,
    deleteEmployeeUseCase,
    setFilterUseCase,
    setSortUseCase,
    createEmployeeUseCase,
    sortModelChangeUseCase,
    loadNextEmployeesUseCase, loadFilteredEmployeesUseCase,
} from "@root/features/Employees/UseCase/EmployeeUseCase";
import {GridFilterModel, GridSortModel} from "@mui/x-data-grid";

export function useEmployeesViewModel(store: IEmployeeStore) {
    const updateEmployee = useCallback((employee: IEmployeeEntity) => {
        return updateEmployeeUseCase(store, employee)
    }, [store])

    const serverUpdateEmployee = useCallback((newEntity: IEmployeeEntity, originEntity: IEmployeeEntity) => {
        return serverUpdateEmployeeUseCase(store, newEntity, originEntity)
    }, [store])
    
    const sortModelChange = useCallback((sortModel: GridSortModel) => {
        return sortModelChangeUseCase(store, sortModel)
    }, [store])

    const loadEmployees = useCallback((pagination?: ILoadingParams) => {
        return loadEmployeesUseCase(store, pagination)
    }, [store])

    const loadFilteredEmployees = useCallback((model: GridFilterModel) => {
        return loadFilteredEmployeesUseCase(store, model)
    }, [store])

    const deleteEmployee = useCallback((employeeId: string) => {
        return deleteEmployeeUseCase(store, employeeId)
    }, [store])

    const createEmployee = useCallback((employee: IEmployeeEntity) => {
        return createEmployeeUseCase(store, employee)
    }, [store])

    const loadNextEmployees = useCallback((pagination: {page: number, pageSize: number}) => {
        return loadNextEmployeesUseCase(store, pagination)
    }, [store])

    const uploadFile = useCallback((file: File) => {
        return uploadFileUseCase(store, file)
    }, [store])

    const setLimit = useCallback((limit: number) => {
        return setLimitUseCase(store, limit)
    }, [store])

    const setFilter = useCallback((filter: Omit<ILoadingParams, 'offset' | 'limit' | 'sortBy' | 'sortOrder'>) => {
        return setFilterUseCase(store, filter)
    }, [store])

    const setSort = useCallback((sort: Omit<ILoadingParams, 'offset' | 'limit' | 'searchString' | 'operator' | 'field'>) => {
        return setSortUseCase(store, sort)
    }, [store])

    const cleanError = useCallback(() => {
        return cleanErrorUseCase(store)
    }, [store])

    const setOffset = useCallback((offset: number) => {
        return setOffsetUseCase(store, offset)
    }, [store])
    
    return {
        showPreloader: store.status === FetchingStatus.Loading,
        offset: store.offset,
        limit: store.limit,
        sortBy: store.sortBy,
        sortOrder: store.sortOrder,
        operator: store.operator,
        searchString: store.searchString,
        field: store.field,
        sortModelChange,
        setFilter,
        loadFilteredEmployees,
        updateEmployee,
        createEmployee,
        loadNextEmployees,
        setSort,
        isEmployeesExists: !!(store.entity && store.entity.length), 
        rowCount: store.entity ? store.entity.length : 0,
        serverUpdateEmployee,
        loadEmployees,
        deleteEmployee,
        setOffset,
        cleanError,
        setLimit,
        uploadFile,
        employees: store.entity,
        isError: store.error != null,
        error: store.error,
    }
}