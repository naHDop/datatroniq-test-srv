export interface IEmployeeEntity {
    id: string
    fullName: string
    jobTitle: string
    department: Departments
    businessUnit: string
    gender: Gender
    ethnicity: Ethnicity
    age: number
    hireDate: Date
    annualSalary: number
    bonus: number
    country: string
    city: string
    exitDate: Date
}

export interface ILoadingParams {
    limit?: number
    offset?: number
    operator?: SearchOperator
    field?: string
    searchString?: string
    sortBy?: string
    sortOrder?: SortOrder
}

export enum SortOrder {
    DESC = "DESC",
    ASC = "ASC"
}

export enum SearchOperator {
    Contains = "contains",
    Equals = "equals",
    StartsWith = "startsWith",
    EndsWith = "endsWith",
    IsEmpty = "isEmpty",
    IsNotEmpty = "isNotEmpty",
    IsAnyOf = "isAnyOf"
}

export enum Departments {
    IT = "IT",
    Finance = "Finance",
    Sales = "Sales",
    Accounting = "Accounting",
    Engineering = "Engineering",
    HumanResources = "Human Resources",
    Marketing = "Marketing"
}

export enum Gender {
    Male = "Male",
    Female = "Female",
    NotSpecified = "NotSpecified"
}

export enum Ethnicity {
    Black = "Black",
    White = "White",
    Asian = "Asian",
    Latino = "Latino",
    Arabian = "Arabian",
    NotSpecified = "NotSpecified",
}