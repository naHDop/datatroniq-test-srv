import {IFetchingStatus} from "@root/store/contract";
import {IEmployeeEntity, ILoadingParams} from "@root/features/Employees/Domain/EmployeeEntity";

export interface IEmployeeStore extends IFetchingStatus, ILoadingParams {
    entity: Array<IEmployeeEntity> | null

    setFilter(filter: Omit<ILoadingParams, 'offset' | 'limit' | 'sortBy' | 'sortOrder'>)
    setSort(sort: Omit<ILoadingParams, 'offset' | 'limit' | 'searchString' | 'operator' | 'field'>)
    uploadFile(file: File)
    setLimit(limit: number)
    setOffset(offset: number)
    loadEmployees(params: ILoadingParams)
    updateEmployee(params: IEmployeeEntity)
    createEmployee(params: IEmployeeEntity)
    deleteEmployee(params: IEmployeeEntity)
    serverUpdateEmployee(params: IEmployeeEntity)
    cleanError()
    // TODO: optional
    // addEmployee()
    // removeEmployee()
}