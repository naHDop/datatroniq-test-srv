import {createAsyncThunk} from "@reduxjs/toolkit";

import {IEmployeeEntity, ILoadingParams} from "@root/features/Employees/Domain/EmployeeEntity";
import {updateEmployeeApi} from "@root/features/Employees/Driver/UpdateEmployeeApi";
import {
    DELETE_EMPLOYEES_THUNK,
    LOAD_EMPLOYEES_THUNK,
    UPDATE_EMPLOYEES_THUNK,
    UPLOAD_EMPLOYEES_FILE,
    CREATE_EMPLOYEES_THUNK
} from "@root/features/Employees/Store/EmployeeContract";
import {loadEmployeeApi} from "@root/features/Employees/Driver/LoadEmployeesApi";
import {uploadEmployeesFileApi} from "@root/features/Employees/Driver/UploadEmployeeFileApi";
import {deleteEmployeeApi} from "@root/features/Employees/Driver/DeleteEmployeeApi";
import {createEmployeeApi} from "@root/features/Employees/Driver/CreateEmployeeApi";

export const updateEmployeeThunk = createAsyncThunk<
    // Return type of the payload creator
    null,
    // First argument to the payload creator
    IEmployeeEntity
>(UPDATE_EMPLOYEES_THUNK, (entity: IEmployeeEntity) => {
    return updateEmployeeApi(entity)
})

export const createEmployeeThunk = createAsyncThunk<
    // Return type of the payload creator
    null,
    // First argument to the payload creator
    IEmployeeEntity
>(CREATE_EMPLOYEES_THUNK, (entity: IEmployeeEntity) => {
    return createEmployeeApi(entity)
})

export const loadEmployeesThunk = createAsyncThunk<
    // Return type of the payload creator
    Array<IEmployeeEntity>,
    // First argument to the payload creator
    ILoadingParams
>(LOAD_EMPLOYEES_THUNK, async (params: ILoadingParams) => {
    return await loadEmployeeApi(params)
})

export const uploadEmployeesFileThunk = createAsyncThunk<
    // Return type of the payload creator
    null,
    // First argument to the payload creator
    File
>(UPLOAD_EMPLOYEES_FILE, async (file: File) => {
    return await uploadEmployeesFileApi(file)
})

export const deleteEmployeesThunk = createAsyncThunk<
    // Return type of the payload creator
    null,
    // First argument to the payload creator
    IEmployeeEntity
>(DELETE_EMPLOYEES_THUNK, async (entity: IEmployeeEntity) => {
    return await deleteEmployeeApi(entity)
})
