import {createSlice, PayloadAction} from "@reduxjs/toolkit";

import {IEmployeeStore} from "@root/features/Employees/Domain/EmployeeStore";
import {FetchingStatus} from "@root/store/contract";
import {EMPLOYEES_SLICE} from "@root/features/Employees/Store/EmployeeContract";
import {RootState} from "@root/store/redux";
import {IEmployeeEntity, ILoadingParams} from "@root/features/Employees/Domain/EmployeeEntity";
import {
    createEmployeeThunk,
    deleteEmployeesThunk,
    loadEmployeesThunk,
    updateEmployeeThunk,
    uploadEmployeesFileThunk
} from "@root/features/Employees/Store/EmployeeThunk";

export type InitialStateType = Omit<
    IEmployeeStore,
    'setFilter' 
    | 'setSort'
    | 'deleteEmployee' 
    | 'cleanError' 
    | 'loadEmployees'
    | 'createEmployee'
    | 'updateEmployee' 
    | 'serverUpdateEmployee' 
    | 'setOffset' 
    | 'setLimit' 
    | 'uploadFile'
>

const initialState: InitialStateType = {
    entity: null,
    error: null,
    status: FetchingStatus.Idle,
    offset: 1,
    limit: 10,
}

export const employeeSlice = createSlice({
    name: EMPLOYEES_SLICE,
    initialState,
    reducers: {
        updateEmployee(state: InitialStateType, action: PayloadAction<IEmployeeEntity>) {
            // Immutable operation
            // https://redux-toolkit.js.org/usage/immer-reducers#immutable-updates-with-immer
            state.entity?.map((em, idx) => {
                if (em.id === action.payload.id && state.entity != null) {
                    state.entity[idx] = action.payload
                }
            })
        },
        setOffset(state: InitialStateType, action: PayloadAction<number>) {
            state.offset = action.payload
        },
        setLimit(state: InitialStateType, action: PayloadAction<number>) {
            state.limit = action.payload
        },
        cleanError(state: InitialStateType) {
            state.error = null
        },
        setFilter(state: InitialStateType, action: PayloadAction<Omit<ILoadingParams, 'offset' | 'limit' | 'sortBy' | 'sortOrder'>>) {
            state.searchString = action.payload.searchString
            state.operator = action.payload.operator
            state.field = action.payload.field
        },
        setSort(state: InitialStateType, action: PayloadAction<Omit<ILoadingParams, 'offset' | 'limit' | 'searchString' | 'operator' | 'field'>>) {
            state.sortOrder = action.payload.sortOrder
            state.sortBy = action.payload.sortBy
        }
    },
    extraReducers: (build) => {
        build
            .addCase(updateEmployeeThunk.pending, (state: InitialStateType) => {
                state.status = FetchingStatus.Loading
            })
            .addCase(updateEmployeeThunk.fulfilled, (state: InitialStateType) => {
                state.status = FetchingStatus.Succeeded
            })
            .addCase(updateEmployeeThunk.rejected, (state: InitialStateType, action) => {
                state.status = FetchingStatus.Failed
                if (action.error?.message) {
                    state.error = action.error.message
                } else {
                    state.error = "Unknown error"
                }
            })

            .addCase(loadEmployeesThunk.pending, (state: InitialStateType) => {
                state.status = FetchingStatus.Loading
            })
            .addCase(loadEmployeesThunk.fulfilled, (state: InitialStateType, action: PayloadAction<Array<IEmployeeEntity>>) => {
                state.status = FetchingStatus.Succeeded
                state.entity = action.payload
            })
            .addCase(loadEmployeesThunk.rejected, (state: InitialStateType, action) => {
                state.status = FetchingStatus.Failed
                if (action.error?.message) {
                    state.error = action.error.message
                } else {
                    state.error = "Unknown error"
                }
            })

            .addCase(uploadEmployeesFileThunk.pending, (state: InitialStateType) => {
                state.status = FetchingStatus.Loading
            })
            .addCase(uploadEmployeesFileThunk.fulfilled, (state: InitialStateType) => {
                state.status = FetchingStatus.Succeeded
            })
            .addCase(uploadEmployeesFileThunk.rejected, (state: InitialStateType, action) => {
                state.status = FetchingStatus.Failed
                if (action.error?.message) {
                    state.error = action.error.message
                } else {
                    state.error = "Unknown error"
                }
            })

            .addCase(createEmployeeThunk.pending, (state: InitialStateType) => {
                state.status = FetchingStatus.Loading
            })
            .addCase(createEmployeeThunk.fulfilled, (state: InitialStateType) => {
                state.status = FetchingStatus.Succeeded
            })
            .addCase(createEmployeeThunk.rejected, (state: InitialStateType, action) => {
                state.status = FetchingStatus.Failed
                if (action.error?.message) {
                    state.error = action.error.message
                } else {
                    state.error = "Unknown error"
                }
            })

            .addCase(deleteEmployeesThunk.pending, (state: InitialStateType) => {
                state.status = FetchingStatus.Loading
            })
            .addCase(deleteEmployeesThunk.fulfilled, (state: InitialStateType) => {
                state.status = FetchingStatus.Succeeded
            })
            .addCase(deleteEmployeesThunk.rejected, (state: InitialStateType, action) => {
                state.status = FetchingStatus.Failed
                if (action.error?.message) {
                    state.error = action.error.message
                } else {
                    state.error = "Unknown error"
                }
            })
    }
})

export const { updateEmployee, setOffset, setLimit, cleanError, setFilter, setSort } = employeeSlice.actions
export const employeeSelector = (state: RootState) => state.employees as InitialStateType;
export default employeeSlice.reducer