import {useCallback} from "react";

import {IEmployeeStore} from "@root/features/Employees/Domain/EmployeeStore";
import {useAppDispatch, useAppSelector} from "@root/store/contract";
import {
    loadEmployeesThunk,
    updateEmployeeThunk,
    uploadEmployeesFileThunk,
    deleteEmployeesThunk, createEmployeeThunk,
} from "@root/features/Employees/Store/EmployeeThunk";
import {IEmployeeEntity, ILoadingParams} from "@root/features/Employees/Domain/EmployeeEntity";
import {
    employeeSelector,
    setLimit as setLimitSlice,
    setOffset as setOffsetSlice,
    updateEmployee as updateEmployeeSlice,
    cleanError as cleanErrorSlice,
    setFilter as setFilterSlice,
    setSort as setSortSlice,
} from "@root/features/Employees/Store/EmployeeSlice";

export function useEmployeesStore(): IEmployeeStore {
    const store = useAppSelector(employeeSelector)
    const {entity, error, status, offset, limit, operator, searchString, field, sortBy, sortOrder } = store
    const dispatch = useAppDispatch()
    
    const loadEmployees = useCallback((params: ILoadingParams) => {
        return dispatch(loadEmployeesThunk(params))
    }, [dispatch])

    const createEmployee = useCallback((entity: IEmployeeEntity) => {
        return dispatch(createEmployeeThunk(entity))
    }, [dispatch])

    const uploadFile = useCallback((file: File) => {
        return dispatch(uploadEmployeesFileThunk(file))
    }, [dispatch])

    const deleteEmployee = useCallback((entity: IEmployeeEntity) => {
        return dispatch(deleteEmployeesThunk(entity))
    }, [dispatch])
    
    const serverUpdateEmployee = useCallback((entity: IEmployeeEntity) => {
        return dispatch(updateEmployeeThunk(entity))
    }, [dispatch])

    const setLimit = useCallback((limit: number) => {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        return dispatch(setLimitSlice(limit))
    }, [dispatch])

    const setFilter = useCallback((filter: Omit<ILoadingParams, 'offset' | 'limit' | 'sortBy' | 'sortOrder'>) => {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        return dispatch(setFilterSlice(filter))
    }, [dispatch])

    const setSort = useCallback((sort: Omit<ILoadingParams, 'offset' | 'limit' | 'searchString' | 'operator' | 'field'>) => {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        return dispatch(setSortSlice(sort))
    }, [dispatch])
    
    const cleanError = useCallback(() => {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        return dispatch(cleanErrorSlice())
    }, [dispatch])

    const setOffset = useCallback((offset: number) => {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        return dispatch(setOffsetSlice(offset))
    }, [dispatch])
    
    const updateEmployee = useCallback((entity: IEmployeeEntity) => {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        return dispatch(updateEmployeeSlice(entity))
    }, [dispatch])

    return {
        loadEmployees,
        setLimit,
        setOffset,
        setFilter,
        cleanError,
        deleteEmployee,
        createEmployee,
        entity,
        operator,
        setSort,
        sortBy,
        sortOrder,
        searchString,
        field,
        error,
        status,
        offset,
        limit,
        updateEmployee,
        uploadFile,
        serverUpdateEmployee,
    }
}