import {AxiosResponse} from "axios";

import {ApiClient} from "@root/clients/api";
import {GenericResponse} from "@root/contracts/clients/api/http";

export function uploadEmployeesFileApi(file: File): Promise<null> {
    const formData = new FormData()
    formData.append('file', file)
    return new Promise((resolve, reject) => {
        ApiClient.post<
            FormData,
            AxiosResponse<GenericResponse<null>>
        >('/v1/employees/upload', formData, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
            })
            .then(() => {
                resolve()
            })
            .catch(err => {
                reject(err)
            })
    })
}
