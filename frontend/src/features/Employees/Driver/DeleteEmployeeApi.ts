import {IEmployeeEntity, ILoadingParams} from "@root/features/Employees/Domain/EmployeeEntity";
import {ApiClient} from "@root/clients/api";
import {AxiosResponse} from "axios";
import {GenericResponse} from "@root/contracts/clients/api/http";

export function deleteEmployeeApi(dto: IEmployeeEntity): Promise<null> {
    return new Promise((resolve, reject) => {
        ApiClient.delete<
            ILoadingParams,
            AxiosResponse<GenericResponse<Array<null>>>
        >('/v1/employee', { data: { id: dto.id }})
            .then(() => {
                resolve()
            })
            .catch(err => {
                reject(err)
            })
    })
}
