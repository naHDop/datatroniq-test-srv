import {AxiosResponse} from "axios";

import {ApiClient} from "@root/clients/api";
import {GenericResponse} from "@root/contracts/clients/api/http";
import {IEmployeeEntity} from "@root/features/Employees/Domain/EmployeeEntity";

export function updateEmployeeApi(dto: IEmployeeEntity): Promise<null> {
    return new Promise((resolve, reject) => {
        ApiClient.patch<
            IEmployeeEntity,
            AxiosResponse<GenericResponse<null>>
        >('/v1/employee', dto)
            .then(() => {
                resolve()
            })
            .catch(err => {
                reject(err)
            })
    })
}
