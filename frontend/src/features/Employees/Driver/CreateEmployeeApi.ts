import {IEmployeeEntity} from "@root/features/Employees/Domain/EmployeeEntity";
import {ApiClient} from "@root/clients/api";
import {AxiosResponse} from "axios";
import {GenericResponse} from "@root/contracts/clients/api/http";

export function createEmployeeApi(dto: IEmployeeEntity): Promise<null> {
    return new Promise((resolve, reject) => {
        ApiClient.post<
            IEmployeeEntity,
            AxiosResponse<GenericResponse<Array<null>>>
        >('/v1/employee', dto)
            .then(() => {
                resolve()
            })
            .catch(err => {
                reject(err)
            })
    })
}
