import {AxiosResponse} from "axios";

import {IEmployeeEntity, ILoadingParams} from "@root/features/Employees/Domain/EmployeeEntity";
import {ApiClient} from "@root/clients/api";
import {GenericResponse} from "@root/contracts/clients/api/http";

export function loadEmployeeApi(dto: ILoadingParams): Promise<Array<IEmployeeEntity>> {
    return new Promise((resolve, reject) => {
        ApiClient.get<
            ILoadingParams,
            AxiosResponse<GenericResponse<Array<IEmployeeEntity>>>
        >('/v1/employees', { params: { ...dto } })
            .then((employees) => {
                resolve(employees.data.data)
            })
            .catch(err => {
                reject(err)
            })
    })
}
