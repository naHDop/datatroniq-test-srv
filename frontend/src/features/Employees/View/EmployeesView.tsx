import React, {JSX, useEffect} from 'react'

import Box from '@mui/material/Box';
import Skeleton from '@mui/material/Skeleton';

import {useEmployeesStore} from "@root/features/Employees/Store";
import {useEmployeesViewModel} from "@root/features/Employees/Controller";
import {StripedDataGrid} from "@ui/StripedDataGrid";
import {getColumns} from "@root/features/Employees/View/table-config";
import {useDebouncedCallback} from "@hooks/Debounce";

const DEBOUNCE_DELAY = 500

export function EmployeesView(): JSX {
    const store = useEmployeesStore()
    const { 
        employees,
        serverUpdateEmployee,
        loadEmployees,
        showPreloader,
        loadNextEmployees,
        deleteEmployee,
        loadFilteredEmployees,
        sortModelChange,
        limit,
        offset,
    } = useEmployeesViewModel(store)

    useEffect(() => {
        loadEmployees()
    }, [])

    const debouncedOnSearch = useDebouncedCallback(loadFilteredEmployees, DEBOUNCE_DELAY)
    const columns = getColumns(deleteEmployee)

    if (!employees && showPreloader) {
        return (
            <Box sx={{ width: '100%' }}>
                <Skeleton height={60} animation="wave" />
                <Skeleton height={60} animation="wave" />
                <Skeleton height={60} animation="wave" />
                <Skeleton height={60} animation="wave" />
                <Skeleton height={60} animation="wave" />
                <Skeleton height={60} animation="wave" />
            </Box>
        )
    }

    return (
        <Box sx={{ height: '80vh', width: '100%' }}>
            <StripedDataGrid
                keepNonExistentRowsSelected
                checkboxSelection
                pagination
                rows={employees || []}
                initialState={{
                    pagination: {
                        paginationModel: {
                            pageSize: limit,
                        },
                    },
                }}
                columns={columns}
                loading={!!employees && showPreloader}
                processRowUpdate={serverUpdateEmployee}
                onProcessRowUpdateError={()=>{}}
                density="comfortable"
                paginationMode="server"
                rowCount={1000}
                pageSizeOptions={[limit]}
                disableRowSelectionOnClick
                getRowClassName={(params) =>
                    params.indexRelativeToCurrentPage % 2 === 0 ? 'even' : 'odd'
                }
                onPaginationModelChange={loadNextEmployees}
                paginationModel={{page: offset, pageSize: limit}}
                filterMode="server"
                onFilterModelChange={debouncedOnSearch}
                sortingMode="server"
                onSortModelChange={sortModelChange}
            />
        </Box>
    )
}