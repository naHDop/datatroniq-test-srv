import {GridColDef, GridValueGetterParams} from "@mui/x-data-grid";
import DeleteIcon from '@mui/icons-material/DeleteOutlined';
import {
    GridActionsCellItem,
} from '@mui/x-data-grid';

export const getColumns: (cd: (id: string) => void) => GridColDef[] = (cb) => Object.freeze([
    { field: 'id', headerName: 'ID', width: 50 },
    {
        field: 'fullName',
        headerName: 'Full name',
        width: 150,
        editable: false,
    },
    {
        field: 'jobTitle',
        headerName: 'Role',
        width: 150,
        editable: true,
    },
    {
        field: 'department',
        headerName: 'Department',
        width: 90,
        editable: true,
    },
    {
        field: 'businessUnit',
        headerName: 'Business Unit',
        width: 150,
        editable: true,
    },
    {
        field: 'gender',
        headerName: 'Gender',
        width: 70,
        editable: true,
    },
    {
        field: 'ethnicity',
        headerName: 'Ethnicity',
        width: 70,
        editable: true,
    },
    {
        field: 'age',
        headerName: 'Age',
        type: 'int',
        width: 40,
        editable: false,
    },
    {
        field: 'hireDate',
        headerName: 'Hire Date',
        type: 'data',
        width: 120,
        editable: false,
    },
    {
        field: 'annualSalary',
        headerName: 'Annual Salary',
        type: 'int',
        width: 100,
        editable: false,
        valueGetter: (params: GridValueGetterParams) => {
            let USDollar = new Intl.NumberFormat('en-US', {
                style: 'currency',
                currency: 'USD',
            });
            
            return USDollar.format(params.row.annualSalary)
        }
    },
    {
        field: 'bonus',
        headerName: 'Bonus',
        type: 'int',
        width: 60,
        editable: false,
    },
    {
        field: 'country',
        headerName: 'Country',
        width: 120,
        editable: true,
    },
    {
        field: 'city',
        headerName: 'City',
        width: 120,
        editable: true,
    },
    {
        field: 'exitDate',
        headerName: 'Exit Date',
        type: 'data',
        width: 100,
        editable: true,
    },
    {
        field: 'actions',
        type: 'actions',
        headerName: 'Actions',
        width: 100,
        cellClassName: 'actions',
        getActions: ({ id }) => {
            return [
                <GridActionsCellItem
                    icon={<DeleteIcon />}
                    label="Delete"
                    onClick={() => cb(id)}
                    color="inherit"
                />
            ]
        },
    }
]);