import { AxiosRequestConfig, AxiosResponse } from 'axios'

export interface IApiClient {
    patch<T, R = AxiosResponse<T>>(
        url: string,
        data?: T,
        config?: AxiosRequestConfig
    ): Promise<R>
    post<T, R = AxiosResponse<T>>(
        url: string,
        data?: T,
        config?: AxiosRequestConfig
    ): Promise<R>
    delete<T, R = AxiosResponse<T>>(url: string, config?: AxiosRequestConfig): Promise<R>
    get<T, R = AxiosResponse<T>>(url: string, config?: AxiosRequestConfig): Promise<R>
}

export interface GenericResponse<T> {
    code: string
    message: string
    timestamp: string
    data: T | null
    error: MSError | null
}

interface MSError {
    message: string
    code: string
}

export enum StatusCode {
    Unauthorized = 401,
    Forbidden = 403,
    TooManyRequests = 429,
    InternalServerError = 500,
}
