export enum ProcessStatus {
    RegistrationIdle = "idle",
    RegistrationPending = "pending",
    RegistrationComplete = "complete",
    RegistrationFailed = "failed",
}
