import { AxiosApiClient } from "./Axios";

const baseUrl = process.env.SERVER_BASE_URL as string
// Singleton
export const ApiClient = new AxiosApiClient(baseUrl)
