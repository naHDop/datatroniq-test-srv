import Axios, {
    AxiosResponse,
    AxiosInstance,
    AxiosRequestConfig,
    InternalAxiosRequestConfig,
    RawAxiosResponseHeaders, AxiosResponseHeaders
} from 'axios'

import {StatusCode, IApiClient, GenericResponse} from '@root/contracts/clients/api/http'
import { XAuthHeader } from "@root/contracts/constants";

const headers = {
    Accept: "application/json",
    "Content-Type": "application/json; charset=utf-8",
};

const injectToken = (config: InternalAxiosRequestConfig): InternalAxiosRequestConfig => {
    try {
        const token = localStorage.getItem("token");

        if (token != null && config.headers) {
            config.headers[XAuthHeader] = `Bearer ${token}`;
        }
        return config;
    } catch (error) {
        throw new Error(error);
    }
};

export class AxiosApiClient implements IApiClient {
    private readonly client: AxiosInstance

    protected createAxiosClient(baseURL: string): AxiosInstance {
        const instance =  Axios.create({
            baseURL,
            responseType: 'json',
            headers,
            timeout: 10 * 1000,
            withCredentials: true,
        })

        instance.interceptors.request.use(injectToken, (error) => Promise.reject(error));
        instance.interceptors.response.use(
            (response) => response,
            (error) => {
                if (Object.keys(error).length === 0) {
                    const er: AxiosResponse<GenericResponse<null>> = {
                        data: {
                            error: { message: error.message },
                        } as GenericResponse<null>,
                        headers: {} as RawAxiosResponseHeaders | AxiosResponseHeaders,
                        config: {} as InternalAxiosRequestConfig,
                        statusText: "Internal error",
                        status: StatusCode.InternalServerError
                    }
                    return  this.handleError(er)
                } else {
                    const { response } = error;
                    return this.handleError(response);
                }
            }
        );

        return instance
    }

    constructor(baseURL: string) {
        this.client = this.createAxiosClient(baseURL)
    }

    get<T, R = AxiosResponse<T>>(url: string, config?: AxiosRequestConfig): Promise<R> {
        return this.client.get<T, R>(url, config);
    }

    post<T, R = AxiosResponse<T>>(
        url: string,
        data?: T,
        config?: AxiosRequestConfig
    ): Promise<R> {
        return this.client.post<T, R>(url, data, config);
    }

    patch<T, R = AxiosResponse<T>>(
        url: string,
        data?: T,
        config?: AxiosRequestConfig
    ): Promise<R> {
        return this.client.patch<T, R>(url, data, config);
    }

    delete<T, R = AxiosResponse<T>>(url: string, config?: AxiosRequestConfig): Promise<R> {
        return this.client.delete<T, R>(url, config);
    }

    private handleError(error: AxiosResponse<GenericResponse<null>>) {
        if (!error) {
            return Promise.reject("Empty error")
        }
        const { status } = error;

        switch (status) {
            case StatusCode.InternalServerError: {
                console.error(error)
                break;
            }
            case StatusCode.Forbidden: {
                console.error(error)
                break;
            }
            case StatusCode.Unauthorized: {
                console.warn(error)
                break;
            }
            case StatusCode.TooManyRequests: {
                console.warn(error)
                break;
            }
        }

        return Promise.reject(error.data.error?.message);
    }
}
