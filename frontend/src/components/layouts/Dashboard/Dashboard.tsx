import React, {PropsWithChildren, JSX, ChangeEvent} from 'react';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';

import {MenuAppBar} from "@root/components/ui/AppBar";

export interface IDashboardLayout {
    onLogout: () => void
    onUpload: (event: ChangeEvent<HTMLInputElement>) => void
    isAuth: boolean
    isEmployeesExists: boolean
}

export function DashboardLayout(props: PropsWithChildren<IDashboardLayout>): JSX {
    const { children, ...rest } = props
    return (
        <Box sx={{ display: 'block' }}>
            {/* Prop drilling is not a good idea */}
            {/* But keep it like this till the better solution */}
            <MenuAppBar {...rest} position="absolute" />
            <Box
                component="main"
                sx={{
                    backgroundColor: (theme) =>
                        theme.palette.mode === 'light'
                            ? theme.palette.grey[100]
                            : theme.palette.grey[900],
                    flexGrow: 1,
                    height: '100vh',
                    overflow: 'auto',
                }}
            >
                <Container maxWidth="lg" sx={{ mt: 4, mb: 4 }}>
                    {children}
                </Container>
            </Box>
        </Box>
    );
}
