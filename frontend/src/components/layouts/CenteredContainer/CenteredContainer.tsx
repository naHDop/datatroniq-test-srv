import React, {JSX, PropsWithChildren} from 'react';
import Container from '@mui/material/Container';
import Box from '@mui/material/Box';

export function CenteredLayoutContainer(props: PropsWithChildren): JSX {
    const { children } = props
    return (
        <Container component="main" maxWidth="xs">
            <Box
                sx={{
                    marginTop: 8,
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                }}
            >
                {children}
            </Box>
        </Container>
    );
}
