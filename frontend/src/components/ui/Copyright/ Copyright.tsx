import React, {MouseEvent} from 'react';
import Typography from '@mui/material/Typography';
import Link from '@mui/material/Link';
import {TypographyOwnProps} from "@mui/material/Typography/Typography";

export interface ICopyrightProps extends TypographyOwnProps {
    readonly redirectTo: () => void
    readonly title: string
}

export function Copyright(props: ICopyrightProps) {
    const { redirectTo, title, ...rest} = props
    const onClickHandler = (event: MouseEvent) => {
        event.preventDefault()
        redirectTo()
    }

    return (
        <Typography variant="body2" color="text.secondary" align="center" {...rest}>
            {'Copyright © '}
            <Link color="inherit" onClick={onClickHandler}>
                {title}
            </Link>{' '}
            {new Date().getFullYear()}
            {'.'}
        </Typography>
    );
}
