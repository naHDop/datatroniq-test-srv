import React, {ChangeEvent} from 'react';

import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import AccountCircle from '@mui/icons-material/AccountCircle';
import MenuItem from '@mui/material/MenuItem';
import Menu from '@mui/material/Menu';
import CloudUploadIcon from '@mui/icons-material/CloudUpload';
import {VisuallyHiddenInput} from "@ui/HiddenFileInput/HiddenFileInput";
import {Button} from "@mui/material";

export interface IMenuAppBarProps {
    onLogout: () => void
    onUpload: (event: ChangeEvent<HTMLInputElement>) => void
    isAuth: boolean
    isEmployeesExists: boolean
}

export function MenuAppBar(props: IMenuAppBarProps) {
    const { onLogout, onUpload, isAuth, isEmployeesExists } = props
    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

    const handleMenu = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorEl(event.currentTarget);
    };
    
    const logoutHandler = () => {
        handleClose()
        onLogout()
    }
    
    const uploadHandler = () => {
        handleClose()
    }

    const handleClose = () => {
        setAnchorEl(null);
    };

    return (
        <Box sx={{ flexGrow: 1 }}>
            <AppBar position="static">
                <Toolbar>
                    <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
                        Dashboard
                    </Typography>
                    {isAuth && (
                        <div>
                            <IconButton
                                size="large"
                                aria-label="account of current user"
                                aria-controls="menu-appbar"
                                aria-haspopup="true"
                                onClick={handleMenu}
                                color="inherit"
                            >
                                <AccountCircle />
                            </IconButton>
                            <Menu
                                id="menu-appbar"
                                anchorEl={anchorEl}
                                anchorOrigin={{
                                    vertical: 'top',
                                    horizontal: 'right',
                                }}
                                keepMounted
                                transformOrigin={{
                                    vertical: 'top',
                                    horizontal: 'right',
                                }}
                                open={Boolean(anchorEl as HTMLElement)}
                                onClose={handleClose}
                            >
                                {!isEmployeesExists && (
                                    <MenuItem onClick={uploadHandler}>
                                        <Button component="label" variant="text" startIcon={<CloudUploadIcon />}>
                                            Upload
                                            <VisuallyHiddenInput accept=".csv,.xlsx" onChange={onUpload} type="file" />
                                        </Button>
                                    </MenuItem>
                                )}
                                <MenuItem onClick={logoutHandler}>Logout</MenuItem>
                            </Menu>
                        </div>
                    )}
                </Toolbar>
            </AppBar>
        </Box>
    );
}