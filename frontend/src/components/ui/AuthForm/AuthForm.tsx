import {ChangeEvent, JSX, MouseEventHandler} from "react";

import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';

import Avatar from '@mui/material/Avatar';
import TextField from '@mui/material/TextField';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Link from '@mui/material/Link';
import Button from '@mui/material/Button';
import {Copyright} from "@ui/Copyright";
import {CenteredLayoutContainer} from "@layouts/CenteredContainer";


export interface IAuthProps {
    header: string
    buttonLabel: string
    isLoading: boolean
    emailValue: string
    passwordValue: string
    isError?: boolean
    error?: string
    authAccountExistsText: string

    icon?: () => JSX
    infoEl?: () => JSX
    onEmailChange: (email: string) => void
    onPasswordChange: (password: string) => void
    onConfirm: () => void
    onNavigate: () => void
}

export function AuthForm(props: IAuthProps) {
    const {
        icon: Icon = LockOutlinedIcon,
        infoEl: Info,
        header,
        onEmailChange,
        onPasswordChange,
        onConfirm,
        buttonLabel,
        isLoading,
        emailValue,
        passwordValue,
        isError = false,
        error = "",
        authAccountExistsText,
        onNavigate
    } = props

    const changeEmailHandler = (event: ChangeEvent<HTMLInputElement>) => {
        onEmailChange(event.target.value.toLowerCase())
    }

    const changePasswordHandler = (event: ChangeEvent<HTMLInputElement>) => {
        onPasswordChange(event.target.value.toLowerCase())
    }

    return (
        <CenteredLayoutContainer>
            <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
                <Icon />
            </Avatar>

            <Typography component="h1" variant="h5">
                {header}
            </Typography>


            <Box noValidate sx={{ mt: 1 }}>
                {Info && <Box sx={{ mt: 2, mb: 2 }}>
                    <Info />
                </Box>}
                <TextField
                    error={isError}
                    value={emailValue}
                    disabled={isLoading}
                    margin="normal"
                    required
                    fullWidth
                    id="email"
                    label="Email Address"
                    name="email"
                    autoComplete="email"
                    autoFocus
                    onChange={changeEmailHandler}
                />
                <TextField
                    error={isError}
                    value={passwordValue}
                    disabled={isLoading}
                    margin="normal"
                    required
                    fullWidth
                    name="password"
                    label="Password"
                    type="password"
                    id="password"
                    autoComplete="current-password"
                    onChange={changePasswordHandler}
                    
                />
                <Button
                    disabled={isLoading}
                    fullWidth
                    variant="contained"
                    sx={{ mt: 3, mb: 2 }}
                    onClick={onConfirm}
                >
                    {buttonLabel}
                </Button>
                <Grid container>
                    <Grid item>
                        <Link sx={{ cursor: 'pointer' }} onClick={onNavigate} variant="body2">
                            {authAccountExistsText}
                        </Link>
                    </Grid>
                </Grid>
            </Box>
            <Box sx={{ marginTop: 8 }}>
                <Copyright redirectTo={() => console.log("Navigate to Copyright")} title="Nft Id Hub" />
            </Box>
        </CenteredLayoutContainer>
    );
}
