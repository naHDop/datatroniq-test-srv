import {useEffect, useState} from 'react'
import { useNavigate, useLocation } from 'react-router-dom';
import * as jose from 'jose'

export const useTokenExpirationEffect = (): boolean => {
    const [isAuth, setIsAuth] = useState(true)
    const location = useLocation()
    const navigateTo = useNavigate()

    useEffect(() => {
        const token = localStorage.getItem('token')
        if (!token) {
            setIsAuth(false)
            navigateTo("/signup")
        }

        const secret = new TextEncoder().encode(
            process.env.JWT_SECRET
        )

        jose.jwtVerify(token, secret)
            .then(({ payload }) => {
                const currentTime = Date.now() / 1000;
                if (payload.exp < currentTime) {
                    setIsAuth(false)
                    localStorage.removeItem("token")
                    navigateTo("/login")
                } else {
                    setIsAuth(true)
                }
            })
            .catch(() => {
                setIsAuth(false)
                localStorage.removeItem("token")
                navigateTo("/login")
            })
    }, [location])
    
    return isAuth
};

