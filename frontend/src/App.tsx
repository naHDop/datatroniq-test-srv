import React, {ChangeEvent} from 'react';
import {Outlet, useNavigate} from "react-router-dom";

import {DashboardLayout} from "@layouts/Dashboard";
import {Copyright} from "@ui/Copyright";
import {useTokenExpirationEffect} from "@hooks/TokenExpiration";
import {useEmployeesStore} from "@root/features/Employees/Store";
import {useEmployeesViewModel} from "@root/features/Employees/Controller";

export function App() {
    const store = useEmployeesStore()
    const { uploadFile, isEmployeesExists } = useEmployeesViewModel(store)
    const isAuth = useTokenExpirationEffect()
    const navigateTo = useNavigate()
    
    const logoutHandler = () => {
        localStorage.removeItem("token")
        navigateTo("/login")
    }

    const uploadHandler = (event: ChangeEvent<HTMLInputElement>) => {
        const file = event.target.files?.[0] || null;
        event.target.value = ''
        if (file) {
            return uploadFile(file)
        }
    }

    return (
        <DashboardLayout isEmployeesExists={isEmployeesExists} onUpload={uploadHandler} onLogout={logoutHandler} isAuth={isAuth} >
            <Outlet />
            <Copyright title="DATATRONiQ" sx={{ pt: 4 }}  redirectTo={() => {}}/>
        </DashboardLayout>
    );
}
