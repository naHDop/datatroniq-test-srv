import React, { StrictMode } from 'react'
import ReactDOM from 'react-dom/client';
import { Provider } from 'react-redux'
import {
    RouterProvider,
} from "react-router-dom";
import { createTheme, ThemeProvider } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import { store } from "@root/store/redux";
import { mainRouter } from "@router/Router";

const root = ReactDOM.createRoot(document.querySelector('#root'));
const defaultTheme = createTheme()

root.render(
    <Provider store={store}>
        <StrictMode>
            <ThemeProvider theme={defaultTheme} >
                <CssBaseline />
                <ToastContainer />
                <RouterProvider router={mainRouter} />
            </ThemeProvider>
        </StrictMode>
    </Provider>
);
