const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

const webpack = require('webpack');
const dotenv = require('dotenv');
const envFilePath = process.env.APP_MODE ? `${process.env.APP_MODE}.env` : 'local.env';
const env = dotenv.config({ path: envFilePath }).parsed;
const cwd = process.cwd();
const envKeys = Object.keys(env).reduce((prev, next) => {
  prev[`process.env.${next}`] = JSON.stringify(env[next]);
  return prev;
}, {});

module.exports = {
  entry: path.resolve(cwd, 'src', 'index.ts'),
  devtool: 'inline-source-map',
  module: {
    rules: [
      {
        test: /\.[jt]sx?$/,
        use: ['babel-loader'],
        exclude: /node_modules/,
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.(?:ico|gif|png|jpg|jpeg)$/i,
        type: 'asset/resource',
      },
      {
        test: /\.(woff(2)?|eot|ttf|otf|)$/,
        type: 'asset/inline',
      },
      {
        test: /\.svg$/,
        use: ['@svgr/webpack'],
      },
    ],
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js', '.jsx'],
    alias: {
      '@root': path.resolve(cwd, 'src'),
      '@layouts': path.resolve(cwd, 'src', 'components', 'layouts'),
      '@ui': path.resolve(cwd, 'src', 'components', 'ui'),
      '@hooks': path.resolve(cwd, 'src', 'hooks'),
      '@router': path.resolve(cwd, 'src', 'router'),
    },
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.resolve(cwd, 'public', 'index.html'),
    }),
    new CleanWebpackPlugin(),
    new webpack.DefinePlugin(envKeys),
  ],
};
