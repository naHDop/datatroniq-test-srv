const path = require("path");

const { merge } = require("webpack-merge");
const common = require("./webpack.common.js");
const cwd = process.cwd();

module.exports = merge(common, {
  mode: "development",
  devtool: "inline-source-map",
  output: {
    publicPath: `http://localhost:3000/`,
  },
  devServer: {
    historyApiFallback: true,
    static: path.join(cwd, "./src"),
    port: 3000,
    hot: "only",
    compress: true,
    open: ["/dashboard"],
    liveReload: true,
  },
});
