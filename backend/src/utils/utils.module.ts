import {Module} from "@nestjs/common";
import {HashService} from "@root/utils/hash.service";

@Module({
    imports: undefined,
    providers: [HashService],
    exports: [HashService]
})
export class UtilsModule {}