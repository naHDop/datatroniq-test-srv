import { Injectable } from '@nestjs/common'

import * as bcrypt from 'bcrypt'

@Injectable()
export class HashService {
    async generateHash(plainPassword: string): Promise<string> {
        return bcrypt.hash(plainPassword, 10)
    }

    async verifyHash(
        plainPassword: string,
        hashedPassword: string
    ): Promise<boolean> {
        return bcrypt.compare(plainPassword, hashedPassword)
    }
}
