import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { ConfigService } from '@nestjs/config';

import { AppModule } from '@root/app.module';
import {HttpExceptionFilter} from "@root/interceptors/error/HttpExceptionFilter";
import {GenericResponseInterceptor} from "@root/interceptors/response/GenericResponseInterceptor";

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    forceCloseConnections: true,
    logger: ['error', 'debug', 'warn', 'log'],
  });

  app.useGlobalPipes(new ValidationPipe({ stopAtFirstError: true }));
  app.useGlobalFilters(new HttpExceptionFilter());
  app.useGlobalInterceptors(new GenericResponseInterceptor());
  app.setGlobalPrefix('api')
  const config: ConfigService = app.get(ConfigService);
  app.enableShutdownHooks();

  app.enableCors({
    origin: config.get<string>('ORIGIN'),
    methods: ['GET', 'POST', 'PATCH', 'DELETE'],
    credentials: true,
    allowedHeaders: ['Access-Control-Allow-Headers', 'Accept', 'Connection', 'Host', 'Content-Length', 'Accept-Encoding', 'X-Auth-Token', 'Content-Type', 'X-CSRF-Token', 'Referer', 'User-Agent', 'Origin']
  })
  
  await app.listen(config.get<string>('APP_PORT'));
  console.log(`Application is running on: ${await app.getUrl()}`);
}
bootstrap();
