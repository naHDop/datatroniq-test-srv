import {
    Body,
    Controller,
    Delete,
    Get,
    Patch,
    Post,
    Query,
    UploadedFile,
    UseGuards,
    UseInterceptors
} from "@nestjs/common";
import { FileInterceptor } from '@nestjs/platform-express';

import {EmployeesFactoryService} from "@employees/factory/employees.factory.service";
import {AuthGuard} from "@interceptors/auth/AuthGuard";
import {CtxUser, UserId} from "@interceptors/auth/user-id.decorator";
import {UsersRepositoryService} from "@root/users/repository/user.repository.service";
import {EmployeeRepositoryService} from "@root/employee/repository/employee.repository.service";
import {NotFoundException} from "@interceptors/error/NotFoundException";
import {InternalErrorException} from "@interceptors/error/InternalErrorException";
import {UpdateEmployeeDto} from "@root/employee/repository/dto/update-employee.dto";
import {DeleteEmployeeDto} from "@root/employee/repository/dto/delete-employee.dto";
import {CreateEmployeeDto} from "@root/employee/repository/dto/create-employee.dto";

@Controller('/v1')
export class EmployeesController {
    constructor(
        private readonly employeesFactoryService: EmployeesFactoryService,
        private readonly userRepositoryService: UsersRepositoryService,
        private readonly employeeRepositoryService: EmployeeRepositoryService
    ) {}
    // @ts-ignore
    @UseGuards(AuthGuard)
    @UseInterceptors(FileInterceptor('file'))
    @Post('/employees/upload')
    async uploadFile(
        @UserId() ctxUser: CtxUser,
        @UploadedFile() file: Express.Multer.File,
    ) {
        if (!file) {
            throw new InternalErrorException("no file")
        }
        const employees = this.employeesFactoryService.getEmployeesFromFile(file)
        const user = await this.userRepositoryService.getUserById(ctxUser.id)
        if (!user) {
            throw new NotFoundException("user not found")
        }

        await this.userRepositoryService.createBulkEmployees(employees, user)
        return
    }

    @UseGuards(AuthGuard)
    @Get('/employees')
    async getEmployees(
        @UserId() ctxUser: CtxUser,
        @Query('offset') offset: string,
        @Query('limit') limit: string,
        @Query('operator') operator?: string,
        @Query('field') field?: string,
        @Query('searchString') searchString?: string,
        @Query('sortBy') sortBy?: "DESC" | "ASC",
        @Query('sortOrder') sortOrder?: string,
    ) {
        const user = await this.userRepositoryService.getUserById(
            ctxUser.id,
            {
                offset: parseInt(offset),
                limit: parseInt(limit),
                operator,
                field,
                searchString,
                sortBy,
                sortOrder,
            }
        )
        if (!user) {
            throw new NotFoundException("user employees not found")
        }
        
        return user.employees
    }
    
    @UseGuards(AuthGuard)
    @Patch('/employee')
    updateEmployee(
        @Body() body: UpdateEmployeeDto
    ) {
        return this.employeeRepositoryService.updateEmployee(body)
    }

    @UseGuards(AuthGuard)
    @Post('/employee')
    async createEmployee(
        @Body() body: CreateEmployeeDto,
        @UserId() ctxUser: CtxUser,
    ) {
        const user = await this.userRepositoryService.getUserById(ctxUser.id)
        if (!user) {
            throw new NotFoundException('user not found')
        }
        return this.employeeRepositoryService.createEmployee(body, user)
    }

    @UseGuards(AuthGuard)
    @Delete('/employee')
    deleteEmployee(
        @Body() body: DeleteEmployeeDto
    ) {
        return this.employeeRepositoryService.deleteEmployee(body.id)
    }
}