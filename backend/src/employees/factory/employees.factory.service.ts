import {Injectable} from "@nestjs/common";
import * as xlsx from 'xlsx';
import { v4 as uuidv4 } from "uuid";

import {CreateEmployeeDto} from "@root/employee/repository/dto/create-employee.dto";
import {Departments, Ethnicity, Gender} from "@employees/employee.interface";
import {InternalErrorException} from "@interceptors/error/InternalErrorException";

@Injectable()
export class EmployeesFactoryService {
    getEmployeesFromFile(file: Express.Multer.File): Array<CreateEmployeeDto> {
        const results: CreateEmployeeDto[] = [];
        const workbook = xlsx.read(file.buffer, { type: 'buffer' });
        const sheetName = workbook.SheetNames[0]; // Assuming the data is in the first sheet

        const sheet = workbook.Sheets[sheetName];
        const jsonData = xlsx.utils.sheet_to_json(sheet);

        jsonData.forEach((row: any) => {
            if (
                !row['Age']
                && !row['Annual Salary']
                && !row['Business Unit']
                && !row['Full Name']
                && !row['Bonus %']
                && !row['City']
                && !row['Country']
                && !row['Department']
                && !row['Ethnicity']
                && !row['Full Name']
                && !row['Gender']
            ) {
                throw new InternalErrorException("wrong fields in file")
            }
            const employee: CreateEmployeeDto = {
                age: parseInt(row['Age']),
                annualSalary: parseInt(row['Annual Salary']),
                bonus: parseFloat(row['Bonus %']),
                businessUnit: row['Business Unit'],
                city: row['City'],
                country: row['Country'],
                department: row['Department'] as Departments,
                ethnicity: row['Ethnicity'] as Ethnicity,
                fullName: row['Full Name'],
                gender: row['Gender'] as Gender,
                hireDate: this.isExcelSerialDate(row['Hire Date']) 
                    ? this.excelSerialDateToJSDate(row['Hire Date']) 
                    : new Date(row['Hire Date']),
                // not uniq
                // id: row['EEID'],
                id: uuidv4(),
                jobTitle: row['Job Title']
            };

            if (row['Exit Date']) {
                employee.exitDate = this.isExcelSerialDate(row['Exit Date']) 
                    ? this.excelSerialDateToJSDate(row['Exit Date']) 
                    : new Date(row['Exit Date'])
            }

            results.push(employee);
        });

        return results;
    }

    private excelSerialDateToJSDate(serialDate) {
        // Microsoft Excel serial dates are days since January 1, 1900 (with a 1900 leap year bug)
        // In JavaScript, January 1, 1900, is day 0, so we subtract 1 from the serialDate
        const millisecondsPerDay = 24 * 60 * 60 * 1000; // Number of milliseconds in a day
        const excelStartDate = new Date(1900, 0, 1); // January is 0-based in JavaScript

        // Calculate the number of days to add
        const daysToAdd = serialDate - 1;

        // Calculate the final JavaScript Date
        return new Date(excelStartDate.getTime() + daysToAdd * millisecondsPerDay);
    }
    
    private isExcelSerialDate(value: any): boolean {
        // Check if the value is a number
        if (typeof value !== 'number') {
            return false;
        }

        // Check if the value falls within a reasonable range for Excel serial dates
        // Excel serial dates are typically in the range 1 (for January 1, 1900) to around 43,000+
        return value >= 1 && value <= 45000;
    }

}