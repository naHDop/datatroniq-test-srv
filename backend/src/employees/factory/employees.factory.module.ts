import {Module} from "@nestjs/common";

import {EmployeesFactoryService} from "@employees/factory/employees.factory.service";

@Module({
    providers: [EmployeesFactoryService],
    exports: [EmployeesFactoryService]
})
export class EmployeesFactoryModule{}