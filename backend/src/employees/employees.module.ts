import {Module} from "@nestjs/common";
import {JwtModule} from "@nestjs/jwt";

import {EmployeesController} from "@employees/employees.controller";
import {EmployeesFactoryModule} from "@employees/factory/employees.factory.module";
import {EmployeesFactoryService} from "@employees/factory/employees.factory.service";
import {AuthModule} from "@root/auth/auth.module";
import {UserRepositoryModule} from "@root/users/repository/user.repository.module";
import {EmployeeRepositoryModule} from "@root/employee/repository/employee.repository.module";

@Module({
    imports:[
        EmployeesFactoryModule,
        AuthModule,
        JwtModule,
        UserRepositoryModule,
        EmployeeRepositoryModule,
    ],
    providers: [
        EmployeesFactoryService,
    ],
    controllers: [EmployeesController],
    exports:[],
})
export class EmployeesModule{}