export interface IEmployeeEntity {
    // own
    id: string
    fullName: string
    age: number
    gender: Gender
    ethnicity: Ethnicity
    country: string
    city: string
    // employer
    jobTitle: string
    department: Departments
    businessUnit: string
    hireDate: Date
    annualSalary: number
    exitDate: Date
    bonus: number
}

export interface ILoadingParams {
    limit?: number
    offset?: number
}

export enum Departments {
    IT = "IT",
    Finance = "Finance",
    Sales = "Sales",
    Accounting = "Accounting",
    Engineering = "Engineering",
    HumanResources = "Human Resources",
    Marketing = "Marketing"
}

export enum Gender {
    Male = "Male",
    Female = "Female",
    NotSpecified = "NotSpecified"
}

export enum Ethnicity {
    Black = "Black",
    White = "White",
    Asian = "Asian",
    Latino = "Latino",
    Caucasian = "Caucasian",
    Arabian = "Arabian",
    NotSpecified = "NotSpecified",
}