import {UtilsModule} from "@root/utils/utils.module";
import {Module} from "@nestjs/common";
import {UsersRepositoryService} from "@root/users/repository/user.repository.service";
import {UserEntity} from "@root/users/repository/user.entity";
import {TypeOrmModule} from "@nestjs/typeorm";
import {EmployeeEntity} from "@root/employee/repository/employee.entity";

@Module({
    imports: [UtilsModule, TypeOrmModule.forFeature([UserEntity, EmployeeEntity])],
    providers: [UsersRepositoryService],
    exports: [UsersRepositoryService, TypeOrmModule, UtilsModule]
})

export class UserRepositoryModule{}