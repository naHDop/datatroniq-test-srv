import {Column, Entity, OneToMany, PrimaryGeneratedColumn, PrimaryColumn} from "typeorm";
import {EmployeeEntity} from "@root/employee/repository/employee.entity";

@Entity('users')
export class UserEntity {
    @PrimaryColumn({ type: 'varchar' })
    id: string

    @Column({ name: 'email', type: 'varchar' })
    email: string

    @Column({ name: 'password', type: 'varchar' })
    hashedPassword: string
    
    @OneToMany(() => EmployeeEntity, (employee) => employee.user)
    employees: Array<EmployeeEntity>
}