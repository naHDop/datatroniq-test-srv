import { v4 as uuidv4 } from "uuid";
import {Repository} from "typeorm";
import {Injectable} from "@nestjs/common";
import {InjectRepository} from "@nestjs/typeorm";


import {UserEntity} from "@root/users/repository/user.entity";
import {CreateUserDto} from "@root/users/repository/dto/create-user.dto";
import { HashService } from "@root/utils/hash.service";
import {CreateEmployeeDto} from "@root/employee/repository/dto/create-employee.dto";
import {EmployeeEntity} from "@root/employee/repository/employee.entity";
import {InternalErrorException} from "@interceptors/error/InternalErrorException";

@Injectable()
export class UsersRepositoryService {
    constructor(
        @InjectRepository(UserEntity)
        private usersRepository: Repository<UserEntity>,
        @InjectRepository(EmployeeEntity)
        private employeeRepository: Repository<EmployeeEntity>,
        private readonly hashService: HashService
    ) {}

    async createBulkEmployees(dto: Array<CreateEmployeeDto>, user: UserEntity): Promise<UserEntity> {
        const employees = dto.map((emp) => {
            const instance = new EmployeeEntity()
            instance.age = emp.age
            instance.annualSalary = emp.annualSalary
            instance.bonus = emp.bonus
            instance.businessUnit = emp.businessUnit
            instance.city = emp.city
            instance.country = emp.country
            instance.department = emp.department
            instance.ethnicity = emp.ethnicity
            instance.exitDate = emp.exitDate
            instance.fullName = emp.fullName
            instance.gender = emp.gender
            instance.hireDate = emp.hireDate
            instance.id = emp.id
            instance.jobTitle = emp.jobTitle
            
            return instance
        })

        await this.employeeRepository.save(employees)
        user.employees = employees

        return await this.usersRepository.save(user)
    }
    
    async createUser(dto: CreateUserDto, withId?: string): Promise<UserEntity> {
        const applicant = await this.getUserByEmail(dto.email)
        if (applicant) {
            throw new InternalErrorException("user exists")
        }
        const hashedPassword = await this.hashService.generateHash(dto.password)
        const newUser = {
            email: dto.email,
            hashedPassword: hashedPassword,
            id: withId ? withId : uuidv4(),
        }
        
        return this.usersRepository.save(newUser)
    }

    async getUserById(id: string, pagination?: { offset: number, limit: number, operator?: string, searchString?: string, field?: string, sortOrder?: string, sortBy?: "DESC" | "ASC" }): Promise<UserEntity | null> {
        if (!pagination) {
            return this.usersRepository.findOne({ where: { id }, relations: { employees: false } });
        }
        
        const {
            limit,
            searchString,
            field,
            operator,
            sortOrder,
            sortBy,
        } = pagination

        let offset = 0
        if (pagination.offset !== 0) {
            offset = (pagination.offset - 1) * limit
        }

        const query = this.usersRepository
            .createQueryBuilder('u')
            .leftJoinAndSelect('u.employees', 'y')
            .offset(offset).limit(limit)
            .where('u.id = :userId and y.deletedAt is null', { userId: id })
        if (operator && searchString && searchString) {
            switch (operator) {
                case 'contains':
                    query.andWhere(`LOWER(y.${field}) LIKE LOWER(:searchString)`, { searchString: `%${searchString}%` });
                    break;
                case 'startsWith':
                    query.andWhere(`LOWER(y.${field}) LIKE LOWER(:searchString)`, { searchString: `${searchString}%` });
                    break;
                case 'endsWith':
                    query.andWhere(`LOWER(y.${field}) LIKE LOWER(:searchString)`, { searchString: `%${searchString}` });
                    break;
                case 'isEmpty':
                    query.andWhere(`LOWER(y.${field}) IS NULL OR y.${field} = LOWER(:searchString)`, { searchString: '' });
                    break;
                case 'isNotEmpty':
                    query.andWhere(`LOWER(y.${field}) IS NOT NULL AND y.${field} != LOWER(:searchString)`, { searchString: '' });
                    break;
                case 'isAnyOf':
                    const searchTermsArray = searchString.split(',').map(term => term.trim());
                    query.andWhere(`LOWER(y.${field}) IN (:...searchTerms)`, { searchTerms: searchTermsArray });
                    break;
                default:
                    query.andWhere(`LOWER(y.${field}) = LOWER(:searchString)`, { searchString });
            }
        }
        
        if (sortOrder && sortBy) {
            query.orderBy(`y.${sortBy}`, sortOrder as "DESC" | "ASC");
        }
        return await query.getOne()
    }

    async getUserByEmail(email: string): Promise<UserEntity | null> {
        return this.usersRepository.findOne({ where: { email }, relations: { employees: false } });
    }
}