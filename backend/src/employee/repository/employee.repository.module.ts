import {Module} from "@nestjs/common";
import {TypeOrmModule} from "@nestjs/typeorm";

import {EmployeeEntity} from "@root/employee/repository/employee.entity";
import {EmployeeRepositoryService} from "@root/employee/repository/employee.repository.service";

@Module({
    imports: [TypeOrmModule.forFeature([EmployeeEntity])],
    providers: [EmployeeRepositoryService],
    exports: [EmployeeRepositoryService, TypeOrmModule]
})

export class EmployeeRepositoryModule{}