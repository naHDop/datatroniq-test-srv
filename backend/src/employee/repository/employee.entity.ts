import {Entity, Column, PrimaryGeneratedColumn, ManyToOne, DeleteDateColumn, PrimaryColumn} from 'typeorm';
import {Departments, Ethnicity, Gender} from "@employees/employee.interface";
import {UserEntity} from "@root/users/repository/user.entity";

@Entity('employees')
export class EmployeeEntity {
    @PrimaryColumn({ type: 'varchar' })
    id: string

    @Column({ name: 'job_title', type: 'varchar' })
    jobTitle: string

    @Column({ type: 'varchar' })
    department: Departments

    @Column({ name: 'business_unit',  type: 'varchar' })
    businessUnit: string

    @Column({ name: 'hire_date', type: 'date' })
    hireDate: Date

    @Column({ name: 'annual_salary', type: 'int' })
    annualSalary: number

    @Column({ name: 'exit_date', type: 'date', nullable: true })
    exitDate: Date

    @Column({ name: 'bonus', type: 'float' })
    bonus: number

    @Column({ name: 'full_name', type: 'varchar' })
    fullName: string

    @Column({ name: 'age', type: 'int' })
    age: number

    @Column({ name: 'gender', type: 'varchar' })
    gender: Gender

    @Column({ name: 'ethnicity', type: 'varchar' })
    ethnicity: Ethnicity

    @Column({ name: 'country', type: 'varchar' })
    country: string

    @Column({ name: 'city', type: 'varchar' })
    city: string
    
    @DeleteDateColumn()
    deletedAt?: Date;
    
    @ManyToOne(() => UserEntity, (user) => user.employees)
    user: UserEntity
}