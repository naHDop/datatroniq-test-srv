import {Repository, UpdateResult} from "typeorm";
import {Injectable} from "@nestjs/common";
import {InjectRepository} from "@nestjs/typeorm";
import { v4 as uuidv4 } from "uuid";

import {UserEntity} from "@root/users/repository/user.entity";
import {CreateEmployeeDto} from "@root/employee/repository/dto/create-employee.dto";
import {EmployeeEntity} from "@root/employee/repository/employee.entity";
import {UpdateEmployeeDto} from "@root/employee/repository/dto/update-employee.dto";

@Injectable()
export class EmployeeRepositoryService {
    constructor(
        @InjectRepository(EmployeeEntity)
        private employeeRepository: Repository<EmployeeEntity>,
    ) {}

    async createEmployee(dto: CreateEmployeeDto, user: UserEntity, withId?: string): Promise<EmployeeEntity> {
        const newEmployee = new EmployeeEntity()
        newEmployee.age = dto.age
        newEmployee.annualSalary = dto.annualSalary
        newEmployee.bonus = dto.bonus
        newEmployee.businessUnit = dto.businessUnit
        newEmployee.city = dto.city
        newEmployee.country = dto.country
        newEmployee.department = dto.department
        newEmployee.ethnicity = dto.ethnicity
        newEmployee.exitDate = dto.exitDate
        newEmployee.fullName = dto.fullName
        newEmployee.gender = dto.gender
        newEmployee.hireDate = dto.exitDate
        newEmployee.id = withId ? withId : uuidv4()
        newEmployee.jobTitle = dto.jobTitle
        newEmployee.user = user
        
        return this.employeeRepository.save(newEmployee)
    }

    async updateEmployee(dto: UpdateEmployeeDto): Promise<EmployeeEntity> {
        const currentEmployee = await this.employeeRepository.findOne({ where: { id: dto.id }})

        currentEmployee.age = dto.age
        currentEmployee.annualSalary = dto.annualSalary
        currentEmployee.bonus = dto.bonus
        currentEmployee.businessUnit = dto.businessUnit
        currentEmployee.city = dto.city
        currentEmployee.country = dto.country
        currentEmployee.department = dto.department
        currentEmployee.ethnicity = dto.ethnicity
        currentEmployee.exitDate = dto.exitDate
        currentEmployee.fullName = dto.fullName
        currentEmployee.gender = dto.gender
        currentEmployee.hireDate = dto.hireDate
        currentEmployee.jobTitle = dto.jobTitle
        
        return this.employeeRepository.save(currentEmployee)
    }

    async deleteEmployee(employeeId: string): Promise<UpdateResult> {
        return this.employeeRepository.softDelete({ id: employeeId })
    }
}