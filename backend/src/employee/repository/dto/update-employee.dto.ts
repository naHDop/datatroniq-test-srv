import {IsDateString, IsEnum, IsNotEmpty, IsNumber, IsString, IsOptional} from "class-validator";
import {Departments, Ethnicity, Gender} from "@employees/employee.interface";

export class UpdateEmployeeDto {
    @IsString()
    @IsNotEmpty()
    id: string

    @IsString()
    @IsNotEmpty()
    fullName: string

    @IsNumber()
    @IsNotEmpty()
    age: number

    @IsEnum(Gender)
    gender: Gender

    @IsEnum(Ethnicity)
    ethnicity: Ethnicity

    @IsString()
    @IsNotEmpty()
    country: string

    @IsString()
    @IsNotEmpty()
    city: string

    @IsString()
    jobTitle: string

    @IsEnum(Departments)
    department: Departments

    @IsString()
    @IsNotEmpty()
    businessUnit: string

    @IsDateString()
    @IsNotEmpty()
    hireDate: Date

    @IsNumber()
    @IsNotEmpty()
    annualSalary: number

    @IsDateString()
    @IsOptional()
    exitDate?: Date

    @IsNumber()
    @IsNotEmpty()
    bonus: number
}
