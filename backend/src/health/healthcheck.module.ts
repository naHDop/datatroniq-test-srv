import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'

import { HealthcheckController } from './healcheck.controller'

@Module({
    imports: [TypeOrmModule],
    controllers: [HealthcheckController],
})
export class HealthcheckModule {}
