import { Controller, Get, ServiceUnavailableException } from '@nestjs/common'
import { Connection } from 'typeorm'

@Controller()
export class HealthcheckController {
    constructor(
        private readonly dbConnect: Connection
    ) {}

    @Get('/liveness')
    async liveness(): Promise<void> {
        // Do nothing
    }

    @Get('/readiness')
    async readiness(): Promise<void> {
        try {
            return this.dbConnect.query('SELECT 1')
        } catch {
            throw new ServiceUnavailableException()
        }
    }
}
