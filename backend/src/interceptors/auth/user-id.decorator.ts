import { createParamDecorator, ExecutionContext } from '@nestjs/common';

export interface CtxUser {
    id: string,
    iat: number,
    exp: number
}

export const UserId = createParamDecorator(
    (data: unknown, ctx: ExecutionContext) => {
        const request = ctx.switchToHttp().getRequest();
        return request.user;
    },
);