import { HttpStatus } from '@nestjs/common';

export interface GenericResponse<T> {
    code: number
    message: string
    timestamp: string
    data: T | null
    error: MSError | null
}

interface MSError {
    message: string
}

export const codeToMessageMap = {
    [HttpStatus.OK]: "OK",
    [HttpStatus.BAD_REQUEST]: "Bad request",
    [HttpStatus.INTERNAL_SERVER_ERROR]: "Failed",
    [HttpStatus.UNAUTHORIZED]: "Unauthorized",
    [HttpStatus.NOT_FOUND]: "Resource not found"
}
