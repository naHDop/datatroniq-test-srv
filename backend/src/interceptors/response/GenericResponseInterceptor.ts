import {
    Injectable,
    NestInterceptor,
    ExecutionContext,
    CallHandler,
    HttpStatus,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {codeToMessageMap, GenericResponse} from "../common.interface";

@Injectable()
export class GenericResponseInterceptor<T> implements NestInterceptor<T, GenericResponse<T>> {
    intercept(context: ExecutionContext, next: CallHandler): Observable<GenericResponse<T>> {
        const now = new Date().toISOString();
        return next.handle().pipe(
            map((data) => ({
                code: HttpStatus.OK,
                message: codeToMessageMap[HttpStatus.OK],
                timestamp: now,
                data: data || null,
                error: null,
            })),
        );
    }
}