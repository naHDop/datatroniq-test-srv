import { ExceptionFilter, Catch, ArgumentsHost, HttpException } from '@nestjs/common';
import { Response } from 'express';

import {codeToMessageMap, GenericResponse} from "../common.interface";

@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter {
    catch(exception: HttpException, host: ArgumentsHost) {
        const ctx = host.switchToHttp();
        const response = ctx.getResponse<Response>();
        const status = exception.getStatus();
        let msg

        if (codeToMessageMap.hasOwnProperty(status)) {
            msg = codeToMessageMap[status]
        } else {
            console.warn("status: ", status)
            msg = "Unknown status error"
        }

        const excResponse = exception.getResponse()
        const generalResp: GenericResponse<null> = {
            code: status, 
            data: null, 
            error: {
                message: excResponse.hasOwnProperty('message') && Array.isArray(excResponse['message']) 
                    ? excResponse['message'][0] 
                    : exception.message,
            }, 
            message: msg, 
            timestamp: new Date().toISOString(),
        }

        response.status(status).json(generalResp);
    }
}