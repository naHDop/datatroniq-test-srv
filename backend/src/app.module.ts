import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from "@nestjs/typeorm";
import { UtilsModule } from "@root/utils/utils.module";
import {UserEntity} from "@root/users/repository/user.entity";
import {EmployeeEntity} from "@root/employee/repository/employee.entity";
import {UserRepositoryModule} from "@root/users/repository/user.repository.module";
import {EmployeesFactoryModule} from "@employees/factory/employees.factory.module";
import {EmployeeRepositoryModule} from "@root/employee/repository/employee.repository.module";
import {EmployeesModule} from "@employees/employees.module";
import {AuthModule} from "@root/auth/auth.module";
import {HealthcheckModule} from "@root/health/healthcheck.module";

const envFilePath = process.env.APP_MODE ? `${process.env.APP_MODE}.env` : 'local.env';

@Module({
  imports: [
      ConfigModule.forRoot({
        isGlobal: true,
        envFilePath: envFilePath,
      }),
      TypeOrmModule.forRootAsync({
          imports: undefined,
          inject: [ConfigService],
          useFactory: (config: ConfigService) => ({
              type: 'postgres',
              host: config.get<string>('DB_HOST'),
              port: config.get<number>('DB_PORT'),
              username: config.get<string>('POSTGRES_USER'),
              password: config.get<string>('POSTGRES_PASSWORD'),
              database: config.get<string>('POSTGRES_DB'),
              entities: [UserEntity, EmployeeEntity],
              // Auth load relations
              autoLoadEntities: true,
              // Auto migration
              synchronize: true,
          })
      }),
      UtilsModule,
      UserRepositoryModule,
      EmployeeRepositoryModule,
      EmployeesFactoryModule,
      EmployeesModule,
      AuthModule,
      HealthcheckModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
