import {Injectable} from "@nestjs/common";
import { JwtService } from '@nestjs/jwt';

import {UsersRepositoryService} from "@root/users/repository/user.repository.service";
import {HashService} from "@root/utils/hash.service";
import {SignupDto} from "@root/auth/dto/signup.dto";
import {LoginDto} from "@root/auth/dto/login.dto";
import {InternalErrorException} from "@interceptors/error/InternalErrorException";
import {NotFoundException} from "@interceptors/error/NotFoundException";

@Injectable()
export class AuthService {
    constructor(
        private readonly userRepositoryService: UsersRepositoryService,
        private readonly hashService: HashService,
        private readonly jwtService: JwtService
    ) {}

    async signup(dto: SignupDto): Promise<{id: string}> {
        const createdUser = await this.userRepositoryService.createUser(dto)
        return { id: createdUser.id }
    }

    async login(dto: LoginDto): Promise<{token: string}> {
        const user = await this.userRepositoryService.getUserByEmail(dto.email)
        if (!user) {
            throw new NotFoundException("user not found")
        }
        const isVerify = await this.hashService.verifyHash(dto.password, user.hashedPassword)
        if (!isVerify) {
            throw new InternalErrorException("wrong credentials")
        }

        const payload = { id: user.id };
        const token = this.jwtService.sign(payload)

        return { token }
    }
}