import {Module} from "@nestjs/common";
import {JwtModule} from '@nestjs/jwt';

import {AuthService} from "@root/auth/auth.service";
import {UtilsModule} from "@root/utils/utils.module";
import {UserRepositoryModule} from "@root/users/repository/user.repository.module";
import {ConfigService} from "@nestjs/config";
import {AppController} from "@root/auth/auth.controller";

@Module({
    imports: [
        UtilsModule,
        UserRepositoryModule,
        JwtModule.registerAsync({
            imports: undefined,
            inject: [ConfigService],
            useFactory: (config: ConfigService) => {
                return {
                    global: true,
                    secret:  config.get<string>('JWT_SECRET'),
                    signOptions: { expiresIn: '4d' },
                }
            }
        }),
    ],
    providers: [AuthService],
    controllers: [AppController],
    exports: [AuthService],
})
export class AuthModule {}