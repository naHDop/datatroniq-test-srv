import {IsEmail, IsString, MaxLength, MinLength} from "class-validator";

export class SignupDto {
    @IsEmail()
    email: string;

    @IsString()
    @MinLength(5)
    @MaxLength(18)
    password: string;
}
