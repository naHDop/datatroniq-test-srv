import {Body, Controller, Post} from "@nestjs/common";
import {AuthService} from "@root/auth/auth.service";
import {SignupDto} from "@root/auth/dto/signup.dto";
import {LoginDto} from "@root/auth/dto/login.dto";

@Controller('/v1')
export class AppController {
    constructor(
        private readonly employeesFactoryService: AuthService
    ) {}
    
    @Post('/signup')
    async signup(
        @Body() body: SignupDto
    ) {
        return this.employeesFactoryService.signup(body)
    }
    
    @Post('/login')
    async login(
        @Body() body: LoginDto
    ) {
        return this.employeesFactoryService.login(body)
    }
}